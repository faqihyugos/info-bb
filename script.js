const params = new URLSearchParams(window.location.search);

const evidence = params.get("evidence");
const perkara = params.get("perkara");
const siapa = params.get("siapa");
const tanggal = params.get("tanggal");
const uraian = params.get("uraian");

const contentTable = `
<table class="table">
<thead>
  <tr>
    <th scope="col" colspan="8" class="align-top text-center">
      <h3>Detail Informasi Barang Bukti </h3>
    </th>
  </tr>
</thead>
<tbody>
  <tr>
    <th scope="row" colspan="2">Index evidence</th>
    <td>:</td>
    <td>${evidence}</td>
  </tr>
  <tr>
    <th scope="row" colspan="2">Disita pada perkara</th>
    <td>:</td>
    <td>${perkara}</td>
  </tr>
  <tr>
    <th scope="row" colspan="2">Disita dari siapa</th>
    <td>:</td>
    <td>${siapa}</td>
  </tr>
  <tr>
    <th scope="row" colspan="2">Tanggal Penyitaan</th>
    <td>:</td>
    <td colspan="2">${tanggal}</td>
  </tr>
  <tr>
    <th scope="row" colspan="2">Uraian Barang Bukti</th>
    <td>:</td>
    <td>
        <p>${uraian}</p>
    </td>
  </tr>
</tbody>
</table>
`;
document.getElementById("content").innerHTML = contentTable;
console.log(evidence);
